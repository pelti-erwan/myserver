# Myserver

## Introduction

Ce projet permet de déployer un serveur Linux.

## Instructions

Clonez le projet dans votre dossier personnel

```
git clone https://gitlab.com
```

Entrez maintenant dans le dossier téléchargé:
    cd myserver

Vous pouvez maintenant:
- installer un serveur web
- installer un serveur ssh


### Installer un serveur web Ngnix

#### Quelque éléments sur le réseau

##### Connaitre son adresse ip

Pour connaitre son adresse ip:
```
ip a
```

On cherche la ligne qui contient inet ('ip a |grep inet').

Ceci est l'adresse de notre ordinateur sur le réseau interne

A ne pas confondre avec votre adresse ip publique, qui est l'adresse de votre BOX, accessible depuis Internet.

Pour obtenir cette dernière, tapez "what is my ip ?" dans la barre de recherche Google

##### Connaittre ses noms d'hôte

On utilise le fichier /etc/hosts`,
qui fonctionne comme un serveur DNS: ils'agit d'un tableau de correspondance
entre des adresses IP et des noms d'hôte.

On va ainsi pouvoir communiquer avec l'ordinateur un utilisant au choix 
l'adresse IP ou le noms d'hôte.

Ex:
```
127.0.0.1       localhost
127.0.1.1       devops14
```

###### Installer nginx

```
sudo apt install nginx
```
Pour vérifier que le serveur fonctionne, on utilise:
```
sudo systemctl status nginx
```
Si le status de nginx est "Inactive", on le démarre avec 
```
sudo systemctl start nginx
```

####### Modifier le contenu

La page par défaut d'accès à nginx se trouve à `/var/www/html/index.nginx.debian.html`

Modifier ce fichier pour changer la page d'accueil.


